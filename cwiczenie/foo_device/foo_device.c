#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/blkdev.h>
#include <linux/uaccess.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include <linux/random.h>

/*
    cat /dev/foo_device

    na rPi: - poziom logowania INFO
    echo 6 > /proc/sys/kernel/printk

    // odczyt czasu
    struct timeval tv;
    struct tm time_hr;
    do_gettimeofday(&tv);

    time_to_tm(tv.tv_sec, 0, &time_hr);
    sprintf(dbuff, "time: %d:%d:%d:%ld ; value: %d\n",
            time_hr.tm_hour, time_hr.tm_min,
            time_hr.tm_sec, tv.tv_usec, readout);



    // generowanie wartosci losowwej
    get_random_bytes(&i, sizeof(i));
    readout = i % 100;

*/

#define DEVICE_NAME "foo_device"
#define CLASS_NAME "foo_device"

static int dev_major;
static struct class* plp_class = NULL;
static struct device* foodevice_dev = NULL;

static ssize_t foodevice_read(struct file* file, char __user *buf, size_t count, loff_t *ppos)
{
}

static struct file_operations foodevice_fops =
{
    .read = foodevice_read,
};

static int __init foodevice_init(void)
{
}

static void __exit foodevice_exit(void)
{
}

module_init(foodevice_init);
module_exit(foodevice_exit);

MODULE_AUTHOR("NSN");
MODULE_DESCRIPTION("foo device");
MODULE_VERSION("0.1");
MODULE_LICENSE("GPL");
