#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

vector<string> read_apps_list()
{
    vector<string> apps;
    cout << "Spawner starting, reading app list" << endl;

    ifstream inp("apps_list.txt");
    string app_name;

    while ( inp >> app_name)
        apps.push_back(app_name);

    cout << "List of controlled apps:" << endl;
    for (const string &app_name : apps)
        cout << app_name << endl;

    return move(apps);
}

int main()
{
    vector<string> apps = read_apps_list();

    for (const string& app : apps)
    {
        cout << "starting " << app << flush;

    }

    while (true)
    {
    }
}
