#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <sys/mman.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include "atomic"
#include <stdexcept>
#include <string>

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

#define MY_PRIORITY     (49)
#define POLLER_PRIORITY (80)
#define SERVER_PRIORITY (90)
#define MAX_SAFE_STACK  (8*1024)
#define NSEC_PER_SEC    (1000000000)

#define DEV_NAME "/dev/foo_device"

unsigned long long interval(500000000); /* 50us*/

bool quit = false;
const int ID_INFO = 10;

void stack_prefault(void)
{

    unsigned char dummy[MAX_SAFE_STACK];

    memset(dummy, 0, MAX_SAFE_STACK);
    return;
}

timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0)
    {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else
    {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

// shared queue - info_queue(ID_INFO);
void send_to_queue(device_message& dmsg)
{
}

void read_from_device()
{
}

void pool_device()
{
}

void configuration_server()
{
}

void setup()
{
}

void set_priority(pthread_t id, int priority, int policy)
{
    sched_param sparam;
    memset(&sparam, 0, sizeof(sparam));
    sparam.sched_priority  = priority;

    pthread_setschedparam(id, policy, &sparam);
}


int main(int argc, char* argv[])
{
    setup();
}

