#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

using namespace std;

void * worker(void * arg)
{
    cout << "Hello from thread :" << *(int*)arg << endl;
    pthread_exit((void*)-1);
}

struct data
{
    int id;
    int err_code;
    char msg[256];
};

void * struct_worker(void * arg)
{
    data* d = (data*)arg;
    cout << "Hello from thread :" << d->id << endl;
    cout << "Message: " << d->msg << endl;
    d->err_code = -1;
    pthread_exit(NULL);
}

int main()
{
    cout << "Hello simple thread!" << endl;
    pthread_t thd;

    int a = 42;
    int err = pthread_create(&thd, NULL, &worker, &a);
    if (err != 0)
        cerr << "Error creating thread" << endl;

    cout << "After launching the thread" << endl;
    int err_code = 0;
    pthread_join(thd, (void**)&err_code);
    cout << "Err code: " << err_code << endl;

    data params;
    params.id = 10;
    char m[] = "Ala ma kota";
    strcpy(params.msg, m);
    err = pthread_create(&thd, NULL, &struct_worker, &params);
    if (err != 0)
        cerr << "Error creating thread" << endl;

    pthread_join(thd, NULL);

    return 0;
}

