#ifndef LOGGER_H
#define LOGGER_H
#include <iostream>
#include <pthread.h>
#include <string>
#include <chrono>
#include "unistd.h"
#include "active_object.h"

using namespace std;

class logger
{
public:
    logger()
    {
    }
    void log(const string& msg)
    {
        auto now_ = chrono::high_resolution_clock::now();
        time_t now = chrono::system_clock::to_time_t(now_);
        string time = ctime(&now);
        time.erase(time.find('\n',0),1);
        sleep(1);
        cout << time << " : " << msg << endl;
    }
};

class active_logger
{
    active_object ao;
public:
    active_logger()
    {
    }
    void log(const string& msg)
    {
        auto now_ = chrono::high_resolution_clock::now();
        time_t now = chrono::system_clock::to_time_t(now_);
        string time = ctime(&now);
        time.erase(time.find('\n',0),1);
        ao.schedule_task( [time, msg] () {
            sleep(1);
            cout << time << " : " << msg << endl;
        } );
    }
};

#endif // LOGGER_H
