#include <iostream>
#include "logger.h"

using namespace std;

int main()
{
    active_logger l;
    l.log("Hello");
    l.log("Second message");
    l.log("Third message");
    l.log("Fourth message");
    cout << "After logging" << endl;
    return 0;
}

