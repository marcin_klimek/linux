#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H
#include <pthread.h>
#include "thread_safe_queue.h"
#include <functional>

typedef std::function<void(void)> task_t;

void * sworker(void *arg);

class active_object
{
    thread_safe_queue<task_t> q;
    pthread_t servant;
    friend void * sworker(void *arg);
public:
    active_object()
    {
        pthread_create(&servant, NULL, sworker, this);
    }
    ~active_object()
    {
        q.push([](){ pthread_exit(NULL);});
        pthread_join(servant, NULL);
    }
    void schedule_task(task_t task)
    {
        q.push(task);
    }
};

void * sworker(void *arg)
{
    active_object* tp = (active_object*)arg;
    while(true)
    {
        task_t task;
        tp->q.pop(task);
        task();
    }
}

#endif // ACTIVE_OBJECT_H
