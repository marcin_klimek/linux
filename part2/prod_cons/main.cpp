#include <iostream>
#include <queue>
#include <pthread.h>
#include <unistd.h>

using namespace std;

queue<int> q;
pthread_mutex_t qmtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void * prod(void * id)
{
    for (size_t i = 0 ; i < 20 ; ++i)
    {
        cout << "produced element " << i << endl;
        pthread_mutex_lock(&qmtx);
        q.push(i);
        pthread_mutex_unlock(&qmtx);
        pthread_cond_signal(&cond); // signaling
    }
}

void * cons(void *id)
{
    for(;;)
    {
        pthread_mutex_lock(&qmtx);
        while (q.empty())
        {
            pthread_cond_wait(&cond, &qmtx);
        }
        int el = q.front();
        q.pop();
        cout << "Got element: " << el << endl;
        pthread_mutex_unlock(&qmtx);
    }
}

int main()
{
    cout << "Hello World!" << endl;
    pthread_t p;
    pthread_t c[4];
    pthread_create(&p, NULL, prod, NULL);
    for (size_t i = 0 ; i < 4 ; ++i)
    {
        pthread_create(&c[i], NULL, cons, NULL);
    }
    for (size_t i = 0 ; i < 4 ; ++i)
    {
        pthread_join(c[i], NULL);
    }

    cons(NULL);
    return 0;
}

