#include <iostream>
#include "inet_sockets.h"
#include <pthread.h>

using namespace std;

void * client(void * arg)
{
    //cout << "Hello Inet socket client!" << endl;

    int fd = sock_connect("localhost", "12345", SOCK_STREAM);
    if (fd == -1)
    {
        cerr << "Connection error" << endl;
        exit(1);
    }

    // we are connected

    int n_read = 0;
    int n_send = 0;
    const size_t BSIZE = 128;
    char buff[BSIZE];

    strncpy(buff, "ala ma kota", sizeof(buff));
    if ( write(fd, buff, strlen(buff)+1) != strlen(buff)+1)
    {
        cerr << "Partial write" << endl;
        exit(1);
    }
    ms_sleep(200);
    memset(buff, 0, sizeof(buff));
    n_read = read(fd, buff, sizeof(buff));
    cout << "got: " << buff << endl;
    close(fd);
}

int main()
{
    const size_t N = 2000;
    pthread_t thds[N];
    for (size_t i = 0 ; i < N ; ++i)
        pthread_create(&thds[i], NULL, client, NULL);
    for (size_t i = 0 ; i < N ; ++i)
        pthread_join(thds[i], NULL);

}

