#include <iostream>
#include <pthread.h>
#include <chrono>

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

unsigned long counter = 0;

using namespace std;

void not_safe()
{
    ++counter;
    if (counter == 19000)
        //throw std::exception();
        pthread_exit(NULL);
}

class lock_guard
{
    pthread_mutex_t &mtx;
public:
    lock_guard(lock_guard& other) = delete;
    void operator=(lock_guard& other) = delete;
    lock_guard(pthread_mutex_t& m) : mtx(m)
    {
        pthread_mutex_lock(&mtx);
    }
    ~lock_guard()
    {
        pthread_mutex_unlock(&mtx);
    }
};

void *inc(void* arg)
{

    for (size_t i = 0 ; i < 20000 ; ++i)
    {
        try
        {
            //pthread_mutex_lock(&mtx);
            {
                lock_guard lock(mtx);
                not_safe();
            }
            //pthread_mutex_unlock(&mtx);
        }
        catch(exception& e)
        {
            cerr << "error" << endl;

        }
     }
}

int main()
{
    const size_t N = 20;
    cout << "Counters!" << endl;
    pthread_t thds[N];

    auto t_start = chrono::high_resolution_clock::now();

    for (size_t i = 0 ; i < N ; ++i)
        pthread_create(&thds[i], NULL, inc, NULL);

    for (size_t i = 0 ; i < N ; ++i)
        pthread_join(thds[i], NULL);

    auto t_end = chrono::high_resolution_clock::now();
    cout << "Elapsed time:";
    cout << chrono::duration_cast<chrono::microseconds>(t_end-t_start).count() << " ms"<< endl;
    cout << "Counter: = " << counter << endl;

    return 0;
}

