#include <iostream>
#include <pthread.h>

using namespace std;

pthread_mutex_t mtx;
pthread_mutexattr_t attr;

void test_err(int err, char* msg)
{
    if (err != 0)
    {
        cerr << "Error: " << msg << endl;
        pthread_exit(NULL);
    }
}

void *worker(void*)
{
    int err;
    cout << "Before mutex 1st lock" << endl;
    err =pthread_mutex_lock(&mtx);
    test_err(err,"First lock");

    cout << "Before mutex 2nd lock" << endl;
    err =pthread_mutex_lock(&mtx);
    test_err(err,"Second lock");

    cout << "Before mutex 1st unlock" << endl;
    err =pthread_mutex_unlock(&mtx);
    test_err(err,"First unlock");

    cout << "Before mutex 2st unlock" << endl;
    err =pthread_mutex_unlock(&mtx);
    test_err(err,"Second unlock");

    cout << "Before mutex 3st unlock" << endl;
    err =pthread_mutex_unlock(&mtx);
    test_err(err,"Third unlock");
}

int main()
{
    pthread_mutexattr_init(&attr);
    //pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_DEFAULT);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    //pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
    pthread_mutex_init(&mtx, &attr);
    pthread_t thd;
    pthread_create(&thd, NULL, worker, NULL);
    pthread_join(thd, NULL);
    return 0;
}

