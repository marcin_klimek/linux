#include <iostream>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

int err_msg(const string& msg)
{
    cout << "ERROR: " << msg << endl;
    exit(1);
}

int main()
{
    cout << "Hello Unix Sockets!" << endl;

    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1)
        err_msg("socket");

    // connection structure
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, "/tmp/sock_test", sizeof(addr.sun_path)-1);

    int err = connect(fd, (sockaddr*)&addr, sizeof(addr));
    if (err == -1)
        err_msg("connect");

    // we are connected
    while(true)
    {
        string line;
        getline(cin, line);
        line += "\n";
        if ( write(fd, line.c_str(), line.size() + 1) != line.size()+1)
        {
            err_msg("partial write");
        }
    }
    close(fd);
    return 0;
}

