#include <iostream>
#include <pthread.h>
#include <unistd.h>

using namespace std;

void * worker(void * arg)
{
    sleep(5);
    cout << "Hello from thread" << endl;
    pthread_exit(NULL);
}

int main()
{
    cout << "Hello simple thread!" << endl;
    pthread_t thd;

    int err = pthread_create(&thd, NULL, &worker, NULL);
    if (err != 0)
        cerr << "Error creating thread" << endl;

    cout << "After launching the thread" << endl;
    pthread_join(thd, NULL);

    return 0;
}

