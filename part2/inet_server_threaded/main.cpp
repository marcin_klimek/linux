#include <iostream>
#include "inet_sockets.h"

using namespace std;

void * client_handle(void* arg)
{
    int cfd = (int)arg;
    int n_read = 0;
    int n_send = 0;
    const size_t BSIZE = 128;
    char buff[BSIZE];

    while ( (n_read = read(cfd, buff, BSIZE)) > 0)
    {
        buff[0] = toupper(buff[0]);
        ms_sleep(100);
        n_send = write(cfd, buff, n_read);
    }

    if (close(cfd) == -1)
    {
        cerr << "closing cfd" << endl;
    }
}

int main()
{
    cout << "Inet Server!" << endl;

    int sfd = sock_listen("12345", 10, NULL);
    if (sfd == -1)
    {
        cerr << "Listen error" << endl;
        exit(1);
    }



    while(true)
    {
        sockaddr_storage client_addr;
        socklen_t size = sizeof(client_addr);
        int cfd = accept(sfd, (sockaddr*)&client_addr, &size); // get client fd
        print_client_info("new client", client_addr);
        if (cfd == -1)
        {
            cerr << "Error in accept" << endl;
            continue;
        }
        pthread_t thd;
        pthread_create(&thd, NULL, client_handle, (void*)cfd);
        pthread_detach(thd);
    }

    return 0;
}

