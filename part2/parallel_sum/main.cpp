#include <iostream>
#include <pthread.h>
#include <chrono>

using namespace std;

typedef unsigned long long num_t;

num_t sum(num_t* begin, num_t* end)
{
    num_t s = 0;
    for (;begin != end ; ++begin)
    {
        s += *begin;
    }
    return s;
}

struct data
{
    num_t* begin;
    num_t* end;
    num_t result;
};

void* worker(void * arg)
{
    data* d = (data*)arg;
    d->result = sum(d->begin, d->end);
    pthread_exit(NULL);
}

num_t parallel_sum(num_t* begin, num_t* end)
{
    const size_t M = 4;
    pthread_t thds[M];
    data ds[M];
    size_t block_size = (end-begin)/M;
    num_t* block_begin = begin;
    for (size_t i = 0 ; i < M ; ++i)
    {
        ds[i].begin = block_begin;
        if (i < M-1)
            ds[i].end = block_begin+block_size;
        else
            ds[i].end = end;
        block_begin += block_size;
        pthread_create(thds+i, NULL, worker, ds+i);
    }
    num_t res = 0;
    for (size_t i = 0 ; i < M ; ++i)
    {
        pthread_join(thds[i], NULL);
        res += ds[i].result;
    }
    return res;
}

int main()
{
    const size_t N = 10000000;
    cout << "Hello Sum!" << endl;
    num_t* table = new num_t[N];
    for (size_t i = 0 ; i < N ; ++i)
    {
        table[i] = i;
    }

    auto t_start = chrono::high_resolution_clock::now();
    cout << "SUM:  = " << sum(table, table+N) << endl;
    auto t_end = chrono::high_resolution_clock::now();
    cout << "Elapsed time:";
    cout << chrono::duration_cast<chrono::microseconds>(t_end-t_start).count() << " ms"<< endl;

    t_start = chrono::high_resolution_clock::now();
    cout << "PSUM: = " << parallel_sum(table, table+N) << endl;
    t_end = chrono::high_resolution_clock::now();
    cout << "Elapsed time:";
    cout << chrono::duration_cast<chrono::microseconds>(t_end-t_start).count() << " ms"<< endl;

    return 0;
}

