#include <iostream>
#include "inet_sockets.h"
#include <sys/epoll.h>
#include "thread_pool.h"

using namespace std;

int efd;

void client_handle(int cfd)
{
    int n_read = 0;
    int n_send = 0;
    const size_t BSIZE = 128;
    char buff[BSIZE];

    while ( (n_read = read(cfd, buff, BSIZE)) > 0)
    {
        buff[0] = toupper(buff[0]);
        ms_sleep(100);
        n_send = write(cfd, buff, n_read);
    }

    if (close(cfd) == -1)
    {
        cerr << "closing cfd" << endl;
    }
    //epoll_ctl(efd, EPOLL_CTL_DEL, cfd, NULL);
}

int main()
{
    cout << "Epoll Server!" << endl;

    int sfd = sock_listen("12345", 10, NULL);
    if (sfd == -1)
    {
        cerr << "Listen error" << endl;
        exit(1);
    }

    efd = epoll_create(1);
    thread_pool tp(100);
    epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = sfd;

    epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &ev);

    epoll_event events[128];

    while(true)
    {
        int activity = epoll_wait(efd, events, 128, -1);
        if (activity == -1)
        {
            cerr << "error on wait" << endl;
            continue;
        }
        for (int i = 0 ; i < activity ; ++i)
        {
            epoll_event& e = events[i];
            if (e.events & EPOLLIN && e.data.fd == sfd)
            {
                sockaddr_storage client_addr;
                socklen_t size = sizeof(client_addr);
                int cfd = accept(sfd, (sockaddr*)&client_addr, &size); // get client fd
                print_client_info("new client", client_addr);
                if (cfd == -1)
                {
                    cerr << "Error in accept" << endl;
                }
                else
                {
                    epoll_event ev;
                    ev.events = EPOLLET | EPOLLIN;
                    ev.data.fd = cfd;
                    epoll_ctl(efd, EPOLL_CTL_ADD, cfd, &ev);
                }
            }
            else
            {
                if (e.events & EPOLLIN)
                {
                    int cfd = e.data.fd;
                    epoll_ctl(efd, EPOLL_CTL_DEL, cfd, NULL);
                    tp.add_task( [cfd] () { client_handle(cfd); } );
                }
                if (e.events & EPOLLHUP || e.events & EPOLLERR)
                {
                    epoll_ctl(efd, EPOLL_CTL_DEL, e.data.fd, NULL);
                }
            }
        }

        //tp.add_task( [cfd] () { client_handle(cfd);});
    }

    return 0;
}

