#include <iostream>
#include <pthread.h>
#include "unistd.h"
#include "launcher.h"

using namespace std;

class Functor
{
    int id;
public:
    Functor(int id) : id(id)
    {
    }
    void operator()()
    {
        cout << "Hello from fctor " << id << endl;
    }
};

class Functorius
{
public:
    Functorius()
    {
    }
    void operator()()
    {
        cout << "Hello from functorious" << endl;
    }
};

void test()
{
    cout << "Hello from test" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    Functor f(10);
    Functorius g;
    //pthread_create()
    pthread_t thdid[3];
    thread_launch2(&thdid[0], f);
    thread_launch_cpp11(&thdid[1], test);
    //thread_launch2(&thdid[2], g);
    thread_launch_cpp11(&thdid[2], []() {cout << "Lambda" << endl;});
    pthread_join(thdid[0], NULL);
    pthread_join(thdid[1], NULL);
    pthread_join(thdid[2], NULL);
    return 0;
}
