#ifndef LAUNCHER_H
#define LAUNCHER_H
#include <pthread.h>
#include <functional>

class AbstractLauncher
{
public:
    virtual ~AbstractLauncher() {}
    virtual void run() = 0;
};

template <class F>
class Launcher : public AbstractLauncher
{
    F f_;
public:
    Launcher(F f) : f_(f)  { }
    void run()
    {
        f_();
    }
};

void * worker(void * arg)
{
    AbstractLauncher* l = (AbstractLauncher*)arg;
    l->run();
    delete l;
}

template <class F>
void thread_launch(pthread_t* id, F fun)
{
    AbstractLauncher* l = new Launcher<F>(fun);
    pthread_create(id, NULL, worker, l);
}

template <class F>
void * worker2(void * arg)
{
    F* l = (F*)arg;
    (*l)();
    delete l;
}

template <class F>
void thread_launch2(pthread_t* id, F fun)
{
    F* l = new F(fun);
    pthread_create(id, NULL, worker2<F>, l);
}

typedef std::function<void(void)> task_t;

void * worker_cpp11(void * arg)
{
    task_t* l = (task_t*)arg;
    (*l)();
    delete l;
}

void thread_launch_cpp11(pthread_t* id, task_t fun)
{
    task_t* l = new task_t(fun);
    int err = pthread_create(id, NULL, worker_cpp11, l);
    if (err !=0)
    {
        delete l;
    }
}

#endif // LAUNCHER_H
