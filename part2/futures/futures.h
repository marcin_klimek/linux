#ifndef FUTURES_H
#define FUTURES_H
#include <pthread.h>
#include <functional>
#include <iostream>

typedef std::function<void(void)> task_t;

void * worker_cpp11(void * arg)
{
    task_t* l = (task_t*)arg;
    (*l)();
    delete l;
}

void thread_launch_cpp11(pthread_t* id, task_t fun)
{
    task_t* l = new task_t(fun);
    int err = pthread_create(id, NULL, worker_cpp11, l);
    if (err !=0)
    {
        delete l;
    }
}

template <typename T>
class ptask
{
    T& result;
    bool done;
    std::function<T(void)> task;
public:
    ptask(std::function<T(void)> task, T& result) : task(task), result(result), done(false)
    {
    }

    void operator ()()
    {
        result = task();
        done = true;
    }
};

template<typename T>
class future
{
    pthread_t id;
    T result;
    ptask<T> task_;

public:
    future(std::function<T(void)> task) : task_(ptask<T>(task, result))
    {
        thread_launch_cpp11(&id, std::move(task_));
    }

    ~future()
    {
    }

    T get()
    {
        pthread_join(id, NULL);
        return result;
    }
};

template<typename T>
future<T> async( std::function<T(void)> f)
{
    return future<T>(f);
}

#endif // FUTURES_H
