#include <iostream>
#include <unistd.h>
//#include <thread>
//#include <future>
#include "futures.h"

using namespace std;

int answer()
{
    sleep(1);
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    future<int> res = async<int>(answer);
    cout << "Waiting for result" << endl;
    cout << res.get() << endl;
    return 0;
}

