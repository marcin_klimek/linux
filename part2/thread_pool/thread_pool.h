#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include "thread_safe_queue.h"
#include <vector>
#include <pthread.h>

typedef std::function<void(void)> task_t;

void * worker (void * arg);

class thread_pool
{
    friend void * worker (void * arg);
    thread_safe_queue<task_t> q;
    std::vector<pthread_t> threads;

public:
    thread_pool(size_t n_of_threads)
    {
        threads.resize(n_of_threads);
        for (pthread_t &thread : threads)
        {
            pthread_create(&thread, NULL, &worker, this);
        }
    }

    ~thread_pool()
    {
        //q.destroy_consumers();
        for (pthread_t &thread : threads)
            q.push([]() { pthread_exit(NULL); });

        for (pthread_t &thread : threads)
        {
            pthread_join(thread, NULL);
        }
    }

    void add_task(task_t task)
    {
        q.push(task);
    }
};

void * worker (void * arg)
{
    thread_pool* tp = (thread_pool*)arg;
    while(true)
    {
        task_t task;
        tp->q.pop(task);
        task();
    }
}

#endif // THREAD_POOL_H
