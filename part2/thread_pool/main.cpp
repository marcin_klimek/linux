#include <iostream>
#include "thread_pool.h"
#include "unistd.h"

using namespace std;

class Functor
{
    int id;
public:
    Functor(int id) : id(id)
    {
    }
    void operator()()
    {
        cout << "Hello from fctor " << id << endl;
    }

    void run()
    {
        cout << "Hello from run " << id << endl;
    }
};

void test()
{
    cout << "Hello from test" << endl;
}

std::function<int(int)> adder(int a)
{
    return [a] (int b) { return a+b;};
}

int main()
{
    cout << "Hello World!" << endl;
    thread_pool tp(4);
    Functor f(10);
    tp.add_task( f );
    tp.add_task( bind(&Functor::run, f) );
    tp.add_task( [&f] () { f.run(); } );
    tp.add_task(test);
    tp.add_task( [] () {
        cout << "Hello from lambda" << endl;}
    );
    int idlambdy = 42;
    tp.add_task( [&idlambdy] () {
        cout << "Hello from lambda ";
        cout << idlambdy << endl;}
    );
    sleep(1);

    auto fu10 = adder(10);
    auto fu20 = adder(20);
    cout << "adder(10)  " << fu10(5) << endl;
    cout << "adder(20)  " << fu20(5) << endl;
    cout << "... " << adder(10)(20) << endl;


    return 0;
}

