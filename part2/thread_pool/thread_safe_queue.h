#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H
#include <iostream>
#include <pthread.h>
#include <queue>

template <typename T>
class thread_safe_queue
{
    bool destroy;
    std::queue<T> q;
    pthread_mutex_t qmtx;
    //pthread_mutex_t d_mtx;
    pthread_cond_t cond;
public:
    thread_safe_queue() :
        destroy(false)
    {
        pthread_mutex_init(&qmtx, NULL);
        //pthread_mutex_init(&d_mtx, NULL);
        pthread_cond_init(&cond, NULL);
    }
    void push(T elem)
    {
        pthread_mutex_lock(&qmtx);
        q.push(elem);
        pthread_mutex_unlock(&qmtx);
        pthread_cond_signal(&cond); // signaling
    }

    void pop(T& elem)
    {
        pthread_mutex_lock(&qmtx);
        while (q.empty() && !destroy)
        {
            pthread_cond_wait(&cond, &qmtx);
        }
        if (destroy)
        {
            std::cout  << "Consumer is going to be killed" << std::endl;
            pthread_mutex_unlock(&qmtx);
            pthread_exit(NULL);
        }
        elem = q.front();
        q.pop();
        pthread_mutex_unlock(&qmtx);
    }

    bool pop_nowait(T& elem)
    {
        pthread_mutex_lock(&qmtx);
        if (q.empty())
        {
            pthread_mutex_unlock(&qmtx);
            return false;
        }
        elem = q.front();
        q.pop();
        pthread_mutex_unlock(&qmtx);
        return true;
    }

    void destroy_consumers(void)
    {
        pthread_mutex_lock(&qmtx);
        destroy = true;
        pthread_cond_broadcast(&cond);
        pthread_mutex_unlock(&qmtx);
    }

    //
    // void task_done();
    // void join_all();
};

#endif // THREAD_SAFE_QUEUE_H
