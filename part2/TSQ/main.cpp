#include <iostream>
#include <queue>
#include <pthread.h>
#include <unistd.h>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<int> q;

void * prod(void * id)
{
    for (size_t i = 0 ; i < 20 ; ++i)
    {
        cout << "produced element " << i << endl;
        q.push(i);
        usleep(100);
    }
    q.destroy_consumers();
}

void * cons(void *id)
{
    for(;;)
    {
        int el;
        q.pop(el);
        cout << "Got element: " << el << "  by: " << (int)id << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    pthread_t p;
    pthread_t c[4];
    pthread_create(&p, NULL, prod, NULL);
    for (size_t i = 0 ; i < 4 ; ++i)
    {
        pthread_create(&c[i], NULL, cons, (void*)i);
    }
    for (size_t i = 0 ; i < 4 ; ++i)
    {
        pthread_join(c[i], NULL);
    }

    cons(NULL);
    return 0;
}

