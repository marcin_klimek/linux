#include <iostream>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

int err_msg(const string& msg)
{
    cout << "ERROR: " << msg << endl;
    exit(1);
}

int main()
{
    cout << "Hello Unix Server!" << endl;

    int sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sfd == -1)
        err_msg("socket");

    // connection structure
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, "/tmp/sock_test", sizeof(addr.sun_path)-1);

    // binding to filepath
    unlink("/tmp/sock_test");
    int err = bind(sfd, (sockaddr*)&addr, sizeof(addr));
    if (err == -1)
        err_msg("bind");

    // listen - passive socket
    if (listen( sfd, 5) == -1)
        err_msg("listen");

    // we are waiting for connections

    int n_read = 0;
    const size_t BSIZE = 128;
    char buff[BSIZE];

    while(true)
    {
        int cfd = accept(sfd, NULL, 0); // get client fd
        if (cfd == -1)
        {
            cerr << "Error in accept" << endl;
            continue;
        }
        while( (n_read = read(cfd, buff, BSIZE)) > 0)
        {
            cout << buff << endl;
        }
        if (close(cfd) == -1)
            err_msg("closing cfd");
    }
    close(sfd);
    return 0;
}

