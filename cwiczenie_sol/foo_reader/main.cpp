#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <sys/mman.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include "launch_thread.h"
#include "shm_queue.h"
#include "net/unix_datagram_socket.h"
#include "atomic"
#include <stdexcept>
#include <string>

using namespace std;

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

#define MY_PRIORITY (49) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */
#define POLLER_PRIORITY 80
#define SERVER_PRIORITY 0

#define MAX_SAFE_STACK (8*1024) /* The maximum stack size which is
                                    guaranteed safe to access without
                                    faulting */

#define NSEC_PER_SEC    (1000000000) /* The number of nsecs per sec. */

#define DEV_NAME "/dev/foo_device"

std::atomic<unsigned long long> interval(500000000); /* 50us*/
bool quit = false;


void stack_prefault(void) {

    unsigned char dummy[MAX_SAFE_STACK];

    memset(dummy, 0, MAX_SAFE_STACK);
    return;
}

timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

shm_queue info_queue(10);
shm_queue error_queue(11);

typedef struct
{
    timespec timestamp;
    int value;
} device_message;

typedef struct
{
    long int type;
    timespec timestamp;
    device_message data;
} queue_message;

void send_to_queue(device_message& dmsg)
{
    queue_message qmsg;
    qmsg.type = 1;
    qmsg.data = dmsg;

    info_queue.enque(&qmsg, sizeof(qmsg));
}

void read_from_device()
{
    device_message message;
    memset(&message, 0, sizeof(message));

    std::cout << "read from device " << std::endl;

    // opening device
    std::ifstream f(DEV_NAME, std::ios::binary);
    if (f.is_open())        
    {        
        f.read(reinterpret_cast<char*>(&message.timestamp.tv_sec), sizeof(message.timestamp.tv_sec));
        f.read(reinterpret_cast<char*>(&message.timestamp.tv_nsec), sizeof(message.timestamp.tv_nsec));
        f.read(reinterpret_cast<char*>(&message.value), sizeof(message.value));
    }

    string time = ctime(&message.timestamp.tv_sec);
    time.erase(time.find('\n',0),1);
    cout << "got time: " << time << " : value: " << message.value << endl;

    std::cout << "send to queue " << std::endl;

    send_to_queue(message);
}


void pool_device()
{
    struct timespec t;
    timespec time_start, time_stop;

    clock_gettime(CLOCK_MONOTONIC ,&t);

    /* start after one second */
    t.tv_sec++;

    while( !quit )
    {
        /* wait until next shot */
        clock_gettime(CLOCK_MONOTONIC, &time_start);
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);
        clock_gettime(CLOCK_MONOTONIC, &time_stop);

        /* do the stuff */
        read_from_device();

        timespec t_tmp = diff(time_start,time_stop);
        std::cout << double(t_tmp.tv_sec) + (double)t_tmp.tv_nsec / 1.0e9 << " s" << std::endl;

        /* calculate next shot */
        t.tv_nsec += interval.load(std::memory_order_relaxed);
        while (t.tv_nsec >= NSEC_PER_SEC) {
            t.tv_nsec -= NSEC_PER_SEC;
            t.tv_sec++;
        }
    }
}

void configuration_server()
{
    net::unix_datagram_socket server_socket;

    server_socket.bind("/tmp/foo-reader-sock");

    char recv_msg[1024];
    char send_msg[1024];
    memset(&send_msg, 0, sizeof(send_msg) );

    while( !quit )
    {
        std::string clientPath;
        server_socket.receiveFrom( recv_msg, sizeof(recv_msg), clientPath);
        std::cout << "recv from " << clientPath << std::endl;

        try
        {
            unsigned long long value = std::stoull( recv_msg );

            if ( value == 0 )
                quit = true;
            else
                interval.store(value, std::memory_order_relaxed);
        }
        catch( std::exception& e)
        {
            std::cout << e.what() << std::endl;
        }

        std::string s = std::string("Current interval = ") + std::to_string(interval);
        server_socket.sendTo( s.data(), s.length()+1, clientPath);
    }
}

void setup()
{
    struct sched_param param;

    /* Declare ourself as a real time task */
    param.sched_priority = MY_PRIORITY;
    if(sched_setscheduler(0, SCHED_FIFO, &param) == -1) {
        perror("sched_setscheduler failed");
        exit(-1);
    }

    /* Lock memory */
    if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) {
        perror("mlockall failed");
        exit(-2);
    }

    /* Pre-fault our stack */
    stack_prefault();
}

void set_priority(pthread_t id, int priority, int policy)
{
    sched_param sparam;
    memset(&sparam, 0, sizeof(sparam));
    sparam.sched_priority  = priority;

    pthread_setschedparam(id, policy, &sparam);
}


int main(int argc, char* argv[])
{
    setup();

    pthread_t pooler_id;
    pthread_t server_id;

    launch_thread(&pooler_id, pool_device);
    launch_thread(&server_id, configuration_server);

    set_priority(pooler_id, POLLER_PRIORITY, SCHED_FIFO);
    set_priority(server_id, SERVER_PRIORITY, SCHED_OTHER);

    pthread_join(pooler_id, NULL);
    pthread_join(server_id, NULL);
}

