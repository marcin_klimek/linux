#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

pid_t spawn( const string& app)
{
    pid_t p;
    switch (p = fork())
    {
    case -1:
    {
        cerr << "Error in fork" << endl;
        return -1;
    }
    case 0:
    {
        execlp(app.data(), app.data());
        cerr << "ERROR in spawn for " << app << endl;
    }
    default:
    {
        return p;
    }
    }
}

vector<string> read_apps_list()
{
    vector<string> apps;
    cout << "Spawner starting, reading app list" << endl;
    ifstream inp("apps_list.txt");
    string app_name;
    while ( inp >> app_name)
        apps.push_back(app_name);

    cout << "List of controlled apps:" << endl;
    for (const string &app_name : apps)
        cout << app_name << endl;
    return move(apps);
}

int main()
{
    vector<string> apps = read_apps_list();
    map<pid_t, string> running;
    for (const string& app : apps)
    {
        cout << "starting " << app << flush;
        running[spawn(app)] = app;
        cout << "... done" << endl;
    }
    while (true) {
        pid_t to_restart = wait(NULL);
        string app_name = running[to_restart];
        cout << "Restarting " << app_name << flush;
        running.erase(to_restart);
        running[ spawn(app_name) ] = app_name;
        cout << "... done" << endl;
    }
}
