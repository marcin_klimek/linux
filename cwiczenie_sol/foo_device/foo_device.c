#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/blkdev.h>
#include <linux/uaccess.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include <linux/random.h>

/*
    cat /dev/foodevice
    echo "FOO" > /dev/foodevice
    sudo sh -c 'echo "sdfsfd" > /dev/foodevice'
*/

#define DEVICE_NAME "foo_device"
#define CLASS_NAME "foo_device"

static int dev_major;
static struct class* plp_class = NULL;
static struct device* foodevice_dev = NULL;

#define CBUFFSIZE 128
char cbuff[CBUFFSIZE];

#define DBUFFSIZE 128
char dbuff[DBUFFSIZE];

struct result_t
{
    struct timespec time;
    int value;
};

static ssize_t foodevice_read(struct file* file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    ssize_t size = 0;
    struct timeval tv;
    //struct tm time_hr;
    unsigned int i, readout;
    struct result_t result;

    printk( KERN_INFO "foodevice, called foodevice_read");

    // get data ready

    do_gettimeofday(&tv);
    //time_to_tm(tv.tv_sec, 0, &time_hr);

    get_random_bytes(&i, sizeof(i));
    readout = i % 100;

    // get structure ready

    /*sprintf(dbuff, "time: %d:%d:%d:%ld ; value: %d\n",
            time_hr.tm_hour, time_hr.tm_min,
            time_hr.tm_sec, tv.tv_usec, readout);*/

    result.time.tv_sec = tv.tv_sec;
    result.time.tv_nsec = tv.tv_usec * 1000;
    result.value = readout;


    //size = simple_read_from_buffer(buf, count, ppos, dbuff, strlen(dbuff));
    size = simple_read_from_buffer(buf, count, ppos, &result, sizeof(result));
    return size;
}


static ssize_t foodevice_write(struct file* file,
	     const char __user *buf, size_t count, loff_t *ppos)
{
    ssize_t size = 0;
    memset(cbuff, 0, CBUFFSIZE);
    printk( KERN_INFO "foodevice_test, called foodevice_write");
    size = simple_write_to_buffer(cbuff, CBUFFSIZE-1, ppos, buf, count);
    //process command
    return size;
}

static struct file_operations foodevice_fops =
{
    .read = foodevice_read,
    .write = foodevice_write
};

static int __init foodevice_init(void)
{
    printk( KERN_INFO "foo devices init");

    dev_major = register_chrdev(0, DEVICE_NAME, &foodevice_fops );  // cat /proc/devices
    if ( dev_major < 0 )
    {
        return -1;
    }

    plp_class = class_create( THIS_MODULE, CLASS_NAME); // /sys/class
    if ( plp_class == NULL )
    {
        // unregister device
        return -1;
    }

    foodevice_dev = device_create( plp_class, NULL, MKDEV(dev_major, 0), NULL, CLASS_NAME ); //
    if ( foodevice_dev == NULL )                                                  // /dev/foodevice_test
    {
        //unregister
        //destroy class
        return -1;
    }


    return 0;
}

static void __exit foodevice_exit(void)
{
    printk( KERN_INFO "foodevice, called foodevice_exit");
    device_destroy( plp_class, MKDEV( dev_major, 0) );
    class_unregister( plp_class );
    class_destroy( plp_class );
    unregister_chrdev( dev_major, DEVICE_NAME );
}

module_init(foodevice_init);
module_exit(foodevice_exit);

MODULE_AUTHOR("Leszek Tarkowski");
MODULE_DESCRIPTION("foo device");
MODULE_VERSION("0.2");
MODULE_LICENSE("GPL");

