#include <string.h>
#include <iostream>
#include <unistd.h>
#include <string>
#include "net/unix_datagram_socket.h"

#define SOCK_ADDR "/tmp/foo-reader-sock"

void configuration_client(char* command)
{
    char recv_msg[1024];
    memset(recv_msg, 0, sizeof(recv_msg));

    try
    {
        net::unix_datagram_socket client_socket;

        client_socket.bind("/tmp/config_client_"+std::to_string( getpid() ));

        client_socket.sendTo(command, strlen(command)+1, SOCK_ADDR );

        int bytes_recv = client_socket.receive(recv_msg, sizeof(recv_msg));

        if(bytes_recv > 0)
            std::cout << "> " << recv_msg << std::endl;

        client_socket.disconnect();
    }
    catch( const net::socket_exception& e)
    {
        std::cerr << e.what() << std::endl;

        return;
    }
}


int main(int argc, char* argv[])
{
    if (argc > 1)
        configuration_client(argv[1]);
    else
    {
        std::cout << "Usage: " << argv[0] << " <interval>" << std::endl;
    }

}

