#include <iostream>
#include "inet_sockets.h"
#include <pthread.h>
#include <string>
#include <chrono>

using namespace std;


void * client(void * arg)
{
    int id = (int)(size_t)arg;
    cout << "Hello Inet socket client!" << id << endl;

    int fd = sock_connect("192.168.2.1", "12345", SOCK_STREAM);
    if (fd == -1)
    {
        cerr << "Connection error" << endl;
        exit(1);
    }

    // we are connected

    int n_read = 0;
    int n_send = 0;

    char message[1024];
    memset(message, 0, sizeof(message));
    n_read = read(fd, &message, sizeof(message));

    cout << "got message: " << id << " - " << message << endl;

    close(fd);
}

int main()
{
    const size_t N = 100;
    pthread_t thds[N];

    for(size_t counter=0; counter<100000; counter++)
    {
        for (size_t i = 0 ; i < N ; ++i)
            pthread_create(&thds[i], NULL, client, (void*)(size_t)i);

        for (size_t i = 0 ; i < N ; ++i)
            pthread_join(thds[i], NULL);
    }
}

