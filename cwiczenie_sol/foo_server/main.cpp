#include <iostream>
#include "inet_sockets.h"
#include <sys/epoll.h>
#include "thread_pool.h"
#include "shm_queue.h"
#include <deque>

using namespace std;

int efd;

const int ID_INFO = 10;


shm_queue info_queue(ID_INFO);

struct device_message
{
    timespec timestamp;
    int value;
};

struct queue_message
{
    long int type;
    timespec timestamp;
    device_message data;
};

pthread_mutex_t mmtx = PTHREAD_MUTEX_INITIALIZER;
device_message message;

device_message get_from_queue()
{
    queue_message qmsg;
    info_queue.deque(&qmsg, sizeof(qmsg), 0);

    device_message message = qmsg.data;

    string time = ctime(&message.timestamp.tv_sec);
    time.erase(time.find('\n',0),1);
    cout << "got time: " << time << " : value: " << message.value << endl;

    return qmsg.data;
}

void update_value()
{
    const int N = 5;
    std::deque<int> avg_set;
    for (int i = 0 ; i < N ; ++i)
        avg_set.push_back(0);
    int avg = 0;

    while(true)
    {
        device_message current_message = get_from_queue();

        //calc running average
        avg_set.push_back(current_message.value);
        avg_set.pop_front();
        avg = 0;
        for (auto &val : avg_set)
            avg += val;
        avg = avg / N;
        // end of running avg

        pthread_mutex_lock(&mmtx);
        message = current_message;
        pthread_mutex_unlock(&mmtx);
    }
}

void client_handle(int cfd)
{
    int n_read = 0;
    int n_send = 0;
    const size_t BSIZE = 128;
    char buff[BSIZE];

    pthread_mutex_lock(&mmtx);
    device_message current_message = message;
    pthread_mutex_unlock(&mmtx);

    string msg = ctime(&current_message.timestamp.tv_sec);
    msg.erase(msg.find('\n',0),1);
    msg += std::string(" - ") + std::to_string(message.value);

    n_send = write(cfd, msg.c_str(), msg.length()+1);

    if (close(cfd) == -1)
    {
        cerr << "closing cfd" << endl;
    }
}

int main()
{
    cout << "Epoll Server!" << endl;

    int sfd = sock_listen("12345", 256, NULL);
    if (sfd == -1)
    {
        cerr << "Listen error" << endl;
        exit(1);
    }

    efd = epoll_create(1);
    thread_pool tp(10);
    tp.add_task( [] () {update_value();});
    epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = sfd;

    epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &ev);

    epoll_event events[32];

    while(true)
    {

        int activity = epoll_wait(efd, events, 32, -1);
        if (activity == -1)
        {
            cerr << "error on wait" << endl;
            continue;
        }
        for (int i = 0 ; i < activity ; ++i)
        {
            epoll_event& e = events[i];
            if (e.events & EPOLLIN && e.data.fd == sfd)
            {
                sockaddr_storage client_addr;
                socklen_t size = sizeof(client_addr);
                int cfd = accept(sfd, (sockaddr*)&client_addr, &size); // get client fd
                
                if (cfd == -1)
                {
                    cerr << "Error in accept" << endl;
                }
                else
                {
                	cout << "new client FD " << cfd << endl;

                    epoll_event ev;
                    ev.events = EPOLLOUT|EPOLLET;
                    ev.data.fd = cfd;
                    epoll_ctl(efd, EPOLL_CTL_ADD, cfd, &ev);
                }
            }
            else
            {
                if (e.events & EPOLLOUT)
                {
                    int cfd = e.data.fd;
                    tp.add_task( [cfd] () { client_handle(cfd); } );

                }
                if (e.events & EPOLLHUP || e.events & EPOLLERR)
                {
                    epoll_ctl(efd, EPOLL_CTL_DEL, e.data.fd, NULL);
                    cout << "hup" << endl;
                }
            }
        }
    }

    return 0;
}

