### Additonal information

Pool:

http://ankieta.infotraining.pl/

### Login and password for VM:

```
devel  /  tymczasowe
devel  /  dev
```

### Reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

### Proxy settings

We can add lines below to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

### Proxy for APT

```
/etc/apt/apt.conf

Acquire::http::proxy "http://10.144.1.10:8080/";
Acquire::https::proxy "https://10.144.1.10:8080/";
Acquire::ftp::proxy "ftp://10.144.1.10:8080/";
```

#### Test polaczenia

```    
curl -v http://www.example.com/
```

### Git repo

git clone https://marcin_klimek@bitbucket.org/marcin_klimek/linux.git

### git

```
git-cheat-sheet
http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

git status  # local repository status
git pull    # getting files from repository
git stash   # moving modifications to stash
```

### Raspberry Pi

Each one has been set for static IP address:

```
iface eth0 inet static
address 192.168.1.1
netmask 255.255.255.0
```

### Ustawienie grupy RT


    Aby móc nadawać wątkom priorytety real-time należy obniżyć limity
    bezpieczeństwa

       twoj_ulubiony_edytor /etc/security/limits.d/99-rt.conf

    w którym ustawiamy limity dla grupy "realtime":

       @realtime   -  rtprio     99
       @realtime   -  memlock    unlimited

    a następnie tworzymy tę grupę i dodajemy naszego użytkownika:

       groupadd realtime
       usermod -a -G realtime twoja_nazwa

       chrt -p PRIORYTET PID
       man chrt




### Dokumentacja

http://python4science.eu

### Budowanie przy uzyciu cmake

1. crosscompile.sh
2. cd <build dir>
3. cmake -D CMAKE_TOOLCHAIN_FILE=<sciezka do pliku pi.cmake> ..


### Linki

    http://www.aristeia.com/Papers/DDJ_Jul_Aug_2004_revised.pdf
    http://www.akkadia.org/drepper/cpumemory.pdf
	
    sortowanie i branch prediction:
	http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array

	what every programmer should know about memory:
	http://lwn.net/Articles/250967/
