#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functional>

using namespace std;

//pid_t spawn( void(*f)(void));

pid_t spawn( function<void(void)> fun)
{
    pid_t p;
    switch (p = fork())
    {
    case -1:
    {
        cerr << "Error in fork" << endl;
        return -1;
    }
    case 0:
    {
        fun();
        exit(0);
    }
    default:
    {
        return p;
    }
    }
}

void test()
{
    sleep(2);
    cout << "Testing spawning" << endl;
    sleep(2);
}

int main()
{
    cout << "Spawn example" << endl;
    pid_t childpid = spawn(test);
    wait(NULL);
    return 0;
}

