#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functional>

using namespace std;

int main()
{
    cout << "Testing exec" << endl;
    pid_t pid = fork();
    if (pid == 0)
    {
        char *argv[] = { "ls", "-l", NULL};
        execvp("ls", argv);
        cerr << "Error executing" << endl;
    }
    else
    {
        cout << "Executed ls" << endl;
        wait(NULL);
    }
    return 0;
}

