#include <iostream>
#include <sched.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <pthread.h>

using namespace std;

int main()
{
    cout << "Simple RT app" << endl;

    struct sched_param params;
    params.sched_priority = 10;
    if (sched_setscheduler(0, SCHED_RR, &params) != 0)
    {
        cerr << "Error setting RT scheduler priority" << endl;
        return 1;
    }

    pthread_t thread;
    thread = pthread_self();
    cpu_set_t cpu;
    CPU_ZERO(&cpu);
    CPU_SET(0, &cpu);
    if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpu) != 0)
    {
        cerr << "Error setting affinity" << endl;
        return 2;
    }

    vector<long> v;
    for (long i = 1000000 ; i > 0 ; --i)
    {
        v.push_back(i);
        sort(v.begin(), v.end());
    }
    return 0;
}

