#include <iostream>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

void handl(int signum)
{
    cout << "Handler called" << endl;
}

int main()
{
    cout << "Hello World!" << endl;

    signal(SIGINT, handl);
    sigset_t set1, set2;

    sigfillset(&set1);
    sigfillset(&set2);
    sigprocmask(SIG_SETMASK, &set1, &set2);
    for(int i = 0 ; i < 10 ; ++i)
    {
        cout << "." << flush;
        sleep(1);
    }
    cout << "end of critical section" << endl;
    sigprocmask(SIG_SETMASK, &set2, NULL);
    return 0;
}

