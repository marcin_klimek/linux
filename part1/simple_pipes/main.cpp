#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <string>

using namespace std;

#define MSG_SIZE 15

char *msg1 = "Ala ma kota";
char *msg2 = "Kot ma Ale";

int main()
{
    int p[2];

    if (pipe(p) == -1)
    {
        cerr << "Error creating pipes" << endl;
    }

    pid_t pid0 = fork();
    if (pid0 == 0)
    {
        close(p[0]);
        dup2(p[1],1);
        close(p[1]);
        const char *const args[] = {"ls", "-l", NULL};
        execvp(args[0],args);
    }

    pid_t pid1 = fork();
    if (pid1 == 0)
    {
        close(p[1]);
        dup2(p[0], 0);
        close(p[0]);
        const char *const args[] = {"wc", NULL};
        execvp(args[0],args);
    }

    close(p[1]);
    close(p[0]);
    wait(NULL);
    wait(NULL);

}

