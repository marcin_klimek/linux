#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    pid_t pid = fork();
    if (pid == 0)
    {
        cout << "inside child!" << endl;
        cout << "getPID: " << getpid() << endl;
        sleep(10);
        return 0;
    }
    else
    {
        cout << "parent, waiting" << endl;
        int status;
        wait(&status);
        return 0;
    }
}

