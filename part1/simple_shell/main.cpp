#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    string line;
    string arg;
    while(true)
    {
        // get line
        cout << "cmd >>> ";
        getline(cin, line);
        // tokenize
        vector<string> vargs;
        char **args;
        istringstream stream(line);
        while (stream >> arg)
        {
            vargs.push_back(arg);
        }
        args = new char*[vargs.size()+1];
        int i = 0;
        for ( ; i < vargs.size() ; ++i)
        {
            args[i] = const_cast<char *>(vargs[i].c_str());
        }
        args[i] = NULL;

        pid_t pid = fork();
        switch (pid)
        {
        case 0:
        {
            execvp(args[0], args);
            cerr << "Command not found" << endl;
            return 1;
        }
        case -1:
        {
            cerr << "Forking error" << endl;
            delete [] args;
            continue;
        }
        default:
        {
            delete [] args;
            wait(NULL);
        }
        }

    }
    return 0;
}

