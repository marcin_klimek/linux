#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>

using namespace std;

int main()
{
    key_t key = 42;
    int shmid = shmget(key, 100, IPC_CREAT | 0666);
    if (shmid == -1)
    {
        cerr << "Error getting shared memory" << endl;
    }

    char *tab;
    tab = (char*)shmat(shmid, 0, 0);
    strcpy(tab, "Leszek");
    cout << tab << endl;
    shmdt(tab);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

