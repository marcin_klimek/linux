#include <iostream>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

extern char **environ;

int main()
{
    cout << "PID:  " << getpid() << endl;
    cout << "PPID: " << getppid() << endl;
    cout << "UID:  " << getuid() << endl;
    cout << "GID:  " << getgid() << endl;
    cout << "eUID: " << geteuid() << endl;
    cout << "eGID: " << getegid() << endl;

//    while(*environ)
//        cout << (*environ++) << endl;

    cout << getenv("LANGUAGE") << endl;
    setenv("QQ", "test", 0);
    cout << getenv("QQ") << endl;

    return 0;
}

