#include "shm_vector.h"
#include <iostream>

shm_vector::shm_vector(key_t shmkey, size_t size)
    : shmkey_(shmkey), size_(size)
{
    lock = new shm_semaphore(shmkey_);
    shmid_ = shmget(shmkey_, sizeof(int)*(size_+1), IPC_EXCL | IPC_CREAT | 0600);
    if (shmid_ == -1)
    {
        if (errno == EEXIST )
        {
            lock->lock();
            std::cout << "Already existed" << std::endl;
            shmid_ = shmget(shmkey_, sizeof(int)*(size_+1), IPC_CREAT | 0600);
            counter = (int*)shmat(shmid_, 0, 0);
            shm_ = counter +1;
            lock->unlock();
        }
    }
    else
    {
        lock->lock();
        std::cout << "First time created" << std::endl;
        counter = (int*)shmat(shmid_, 0, 0);
        shm_ = counter + 1;
        (*counter) = 0;
        lock->unlock();
    }
}


shm_vector::~shm_vector()
{
    shmdt(counter);
}


void shm_vector::push_back(int element)
{
    lock->lock();
    shm_[*counter] = element;
    ++(*counter);
    lock->unlock();
}

size_t shm_vector::size()
{
    return *counter;
}

bool shm_vector::empty()
{
    return !(*counter);
}

int &shm_vector::operator[](size_t index)
{
    lock->lock();
    int &val = shm_[index];
    lock->unlock();
    return val;
}

const int &shm_vector::operator[](size_t index) const
{
    return shm_[index];
}

void shm_vector::clean()
{
    lock->lock();
    (*counter) = 0;
    lock->unlock();
}

void shm_vector::destroy()
{
    shmctl(shmid_, IPC_RMID, NULL);
    lock->destroy();
}
