#include <iostream>
#include "shm_queue.h"
#include "shm_semaphore.h"
#include "shm_vector.h"
#include <sys/wait.h>
#include <unistd.h>
#include "spawn.h"

#define ipc 0x20
using namespace std;

shm_queue q(ipc);
shm_vector vec(ipc, 1000);

bool is_prime(int n)
{
    for(int d=2; d<n; ++d)
        if( n % d == 0)
            return false;

    return true;
}

void producer()
{
    for (int i = 2 ; i < 100 ; ++i)
    {
        q.enque(i);
        cout << "produced " << i << endl;
        sleep(1);
    }
    q.enque(-1);
}

void consumer()
{
    while(true)
    {
        int i = q.deque();
        if (i != -1)
        {
            cout << i << " consumed by: " << getpid() << endl;
            if (is_prime(i))
                vec.push_back(i);
        }
        else
        {
            q.enque(-1);
            return;
        }
    }
}

void printer()
{
    int size = 0;
    for (int i = 0 ; size < vec.size() ; ++size)
    {
        cout << vec[size] << " " << flush;
    }
}

int main()
{
    pid_t prod = spawn(&producer);
    pid_t con1 = spawn(&consumer);
    pid_t con2 = spawn(&consumer);
    wait(NULL);
    wait(NULL);
    wait(NULL);
    printer();
    q.destroy();
    vec.destroy();

/*        q.enque(100);
        q.enque(200);
        q.enque(300);
        cout << q.deque() << " : " << q.deque() << ":" << q.deque() << endl;
        q.destroy();
    //    cout << "SEM before lock: " << sem.get_value() << endl;
    //    sem.lock();
    //    cout << "SEM after lock: " << sem.get_value() << endl;
    //    sem.unlock();
    //    cout << "SEM after unlock: " << sem.get_value() << endl;
    //    //sem.destroy();

        vec.push_back(10);
        vec.push_back(20);
        vec.push_back(30);
        for (int i = 0 ; i < vec.size() ; ++i)
        {
            cout << vec[i] << endl;
        }
        //vec.destroy()*/;
    return 0;
}
