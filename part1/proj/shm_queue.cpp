#include "shm_queue.h"
#include <iostream>

shm_queue::shm_queue(key_t shqkey) : ipckey_(shqkey)
{
    if ((msqid_ = msgget(ipckey_, IPC_CREAT | 0600)) == -1)
    {
        std::cerr << "Error creating queue" << std::endl;
        throw std::exception();
    }
}

shm_queue::~shm_queue()
{

}

void shm_queue::enque(int value)
{
    message_int msg;
    msg.type = 1;
    msg.data = value;
    if (msgsnd( msqid_, &msg, sizeof(msg), 0) == -1)
    {
        std::cerr << "Error enqueing" << std::endl;
        throw std::exception();
    }
}

int shm_queue::deque()
{
    message_int msg;
    if (msgrcv( msqid_, &msg, sizeof(msg), 1, 0) == -1)
    {
        std::cerr << "Error dequeing" << std::endl;
        throw std::exception();
    }
    return msg.data;
}

void shm_queue::destroy()
{
    msgctl(msqid_, IPC_RMID, NULL);
    msqid_ = 0;
}
