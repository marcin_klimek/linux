#ifndef SPAWN_H
#define SPAWN_H
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <stdexcept>

pid_t spawn( void (*fun)(void) )
{
    pid_t p;
    switch( p = fork() )
    {
    case -1:
    {
        std::cerr << "ERROR - fork fail" << std::endl;        
        throw std::bad_alloc();
    }
    case 0: // spawn - run function, then exit
    {
        fun();
        exit(0);
    }
    default: // parent - return pid
    {
        return p;
    }
    }
}

#endif // SPAWN_H
