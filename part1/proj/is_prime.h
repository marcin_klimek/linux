#pragma once

bool is_prime(unsigned long v)
{
    for(int d=2; d<v; ++d)
        if( v % d == 0)
            return false;

    return true;
}