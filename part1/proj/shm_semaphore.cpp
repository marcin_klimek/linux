#include "shm_semaphore.h"
#include <iostream>

shm_semaphore::shm_semaphore(key_t semkey) : semkey_(semkey)
{
    if ( (semid_ = semget(semkey_, 1, 0666 | IPC_CREAT | IPC_EXCL)) == -1)
    {
        if (errno == EEXIST) // already exists
        {
            std::cout << "reusing semaphore" << std::endl;
            if ((semid_ = semget(semkey_, 1, 0666 | IPC_CREAT)) == -1)
            {
                std::cerr << "Error creating semaphore 1" << std::endl;
                throw std::exception();
            }
            else
            {
                return;
            }
        }
        else
        {
            std::cerr << "Error creating semaphore 2" << std::endl;
            throw std::exception();
        }
    }
    else // first time created
    {
        std::cout << "new semaphore" << std::endl;
        semun setops;
        setops.val = 1;
        semctl( semid_, 0, SETVAL, setops);
    }
}

void shm_semaphore::lock()
{
    struct sembuf p_buf;
    p_buf.sem_num = 0;
    p_buf.sem_op = -1;
    p_buf.sem_flg = SEM_UNDO;

    //std::cerr << "locking semaphore" << std::endl;
    if (semop(semid_, &p_buf, 1) == -1)
    {
        std::cerr << "error setting semaphore" << std::endl;
        throw std::exception();
    }
}

void shm_semaphore::unlock()
{
    struct sembuf p_buf;
    p_buf.sem_num = 0;
    p_buf.sem_op = 1;
    p_buf.sem_flg = SEM_UNDO;

    if (semop(semid_, &p_buf, 1) == -1)
    {
        std::cerr << "error setting semaphore";
        throw std::exception();
    }
}

shm_semaphore::~shm_semaphore()
{

}

void shm_semaphore::destroy()
{
    semctl( semid_, IPC_RMID, NULL);
}

int shm_semaphore::get_value()
{
    return semctl( semid_, 0, GETVAL, 0);
}
