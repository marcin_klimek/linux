#ifndef SHM_SEMAPHORE_H
#define SHM_SEMAPHORE_H
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <errno.h>
#include <stdexcept>

typedef union _semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
} semun;

class shm_semaphore
{
    key_t semkey_;
    int semid_;
    u_int numsem_;

public:
    shm_semaphore(key_t semkey);
    void lock();
    void unlock();
    ~shm_semaphore();
    void destroy();
    int get_value();
};

#endif // SHM_SEMAPHORE_H
