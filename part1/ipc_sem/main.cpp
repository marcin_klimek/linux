#include <iostream>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

typedef union _semun
{
    int val;
    struct semid_ds* buf;
    unsigned short *array;
} semun;

using namespace std;

int main()
{
    key_t key = 42;
    int semid = semget(key, 1, IPC_EXCL | IPC_CREAT | 0666);
    if (semid == -1)
    {
        if (errno == EEXIST )
        {
            cout << "Already existed" << endl;
            semid = semget(key, 1, IPC_CREAT | 0666);
        }
    }

    semun semops;
    semops.val = 1;
    semctl(semid, 0, SETVAL, semops);

    struct sembuf p_buf;
    p_buf.sem_num = 0;
    p_buf.sem_op = -1;
    p_buf.sem_flg = SEM_UNDO;
    if( semop(semid, &p_buf, 1) == -1 )
    {
        cerr << "Semaphore op error" << endl;
    }

    p_buf.sem_op = 1;

    if( semop(semid, &p_buf, 1) == -1 )
    {
        cerr << "Semaphore op error" << endl;
    }

    if( semop(semid, &p_buf, 1) == -1 )
    {
        cerr << "Semaphore op error" << endl;
    }

    cout << semid << endl;

    semctl(semid, 0, IPC_RMID);

    return 0;
}

