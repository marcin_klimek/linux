#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

using namespace std;

struct msg
{
    long type;
    char data[128];
};

int main()
{
    key_t key = 42;
    int msgid = msgget(key, IPC_CREAT | 0666);
    if (msgid == -1)
    {
        cerr << "Error openign queue" << endl;
    }
    msg packet;
//    packet.type = 1;
//    strcpy(packet.data, "Leszek");
//    msgsnd(msgid, &packet, sizeof(packet), 0);
    //msgrcv(msgid, &packet, sizeof(packet), 1, 0);
    struct msqid_ds ds;
    msgctl(msgid, IPC_STAT, &ds);
    cout << "Lenght of q " << ds.msg_qbytes << endl;
    return 0;
}

