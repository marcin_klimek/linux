#include <iostream>
#include <stdlib.h>
#include <string>
#include <string.h>

using namespace std;

#define hMB 1024*1024*100
#define N 10000

int main()
{
    cout << "Starting alloc" << endl;
    char *mem[N];

    for (int i = 0 ; i < N ; ++i)
    {
        mem[i] = (char*)malloc(hMB);
        if (mem[i] == 0)
        {
            cerr << "Error allocating at " << i/10.0 << " GB" << endl;
            return 1;
        }
        else
        {
            //memset(mem[i], 0, hMB);
            cout << "Allocated " << i/10.0 << " GB" << endl;
        }

    }

    string temp;
    cin >> temp;

    return 0;
}

