#!/bin/bash

export ARCH=arm
export CROSS_COMPILE=$HOME/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-
export PATH=$PATH:$HOME/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin

arm-linux-gnueabihf-gcc demo.c -I . -L . libbcm2835.a --sysroot=${HOME}/rpi/rootfs -lrt -o demo