#!/bin/bash
export ARCH=arm
export CROSS_COMPILE=$HOME/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-
export PATH=$PATH:$HOME/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin
env PS1="crosscompiler@\h \w>: " /bin/bash --norc -i
