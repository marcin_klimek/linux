#include <iostream>
#include <atomic>
#include <thread>
#include <unistd.h>
#include <atomic>
#include <vector>

using namespace std;

const unsigned int NUM_ITER=100000;
std::atomic<long int> counter;

void foo()
{
    usleep(10000);

    std::cout << "thread" << std::endl;
}

void bar()
{
    for(int i=0; i<NUM_ITER; ++i)
        counter++;
}

int main()
{
    std::vector<std::thread> grp;
    grp.push_back(std::thread(bar));
    grp.push_back(std::thread(bar));
    grp.push_back(std::thread(bar));

    cout << "Hello World! 1234" << endl;

    for(std::thread& t : grp)
        t.join();

    cout << "Counter " << counter << endl;

    return 0;
}

