#include <atomic>
#include <iostream>
#include <thread>

class spinlock_mutex
{
    std::atomic_flag flag;

public:
    spinlock_mutex() : flag(ATOMIC_FLAG_INIT)
    {}

    void lock()
    {
        while( flag.test_and_set() );
    }

    void unlock()
    {
        flag.clear();
    }
};

spinlock_mutex mtx;

void foo(int id)
{
    while(true)
    {
        std::cout << id << " before lock" << std::endl;
        mtx.lock();

        std::this_thread::sleep_for( std::chrono::seconds(1) );

        mtx.unlock();
        std::cout << id << " after lock" << std::endl;
    }
}


int main()
{
    std::thread t1(foo, 1);
    std::thread t2(foo, 2);

    t1.join();
    t2.join();


    std::atomic<int> value;

    value.fetch_add(1);  // --> value++

    int fetch_add(int v)
    {
        prev_value = value;
        value = value + v;
        return prev_value;
    }


    value.load();     // x = value;
    value.store(42);  // value = 42;

}
