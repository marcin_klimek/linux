#include <iostream>
#include <atomic>
#include <thread>

using namespace std;

class spin_barrier
{
    std::atomic<unsigned int> num_wait;
    std::atomic<unsigned int> generation;
    int barrier;



public:

    spin_barrier(int value):  num_wait(0), generation(0), barrier(value) {}

    void wait()
    {
        int current_generation = generation.load();

        if ( num_wait.fetch_add(1) == (barrier - 1))
        {
            num_wait.store(0);
            generation.fetch_add(1);
        }
        else
        {
            while( current_generation == generation.load() );
        }
    }
};

spin_barrier b(2);

namespace atom
{
    std::atomic<bool> x,y;
    int z;

    void write_x_than_y()
    {
        b.wait();
        x.store(true, std::memory_order_relaxed);
        y.store(true, std::memory_order_relaxed);
    }

    void read_y_than_x()
    {
        b.wait();

        while( !y.load(std::memory_order_relaxed) );

        if(x.load(std::memory_order_relaxed))
            ++z;
    }

}

namespace norm
{
    bool x,y;
    int z;

    void write_x_than_y()
    {
        b.wait();

        x = true;
        y = true;
    }

    void read_y_than_x()
    {
        b.wait();

        while( !y );

        if(x)
            ++z;
    }
}

using namespace norm;

int main()
{
    long counter = 0;
    while(true)
    {
        x = false;
        y = false;
        z = 0;

        std::thread a( write_x_than_y );
        std::thread b( read_y_than_x );

        a.join();
        b.join();

        if (counter % 100 == 0)
            std::cout << counter << " " << z << std::endl;

        if (z==0)
            break;

        counter++;
    }
    std::cout << counter << " " << z << std::endl;

    return 0;
}

