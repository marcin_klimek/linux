#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>

using namespace std;

const int loop_count = 10000000;

int counter;

void worker_norm()
{
    for(int i=0; i<loop_count; i++)
    {
        counter++;
    }
}

int test_norm()
{
    counter = 0;

    auto start = std::chrono::high_resolution_clock::now();
    std::thread t1(worker_norm);
    std::thread t2(worker_norm);

    t1.join();
    t2.join();

    auto end = std::chrono::high_resolution_clock::now();

    int time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

    std::cout << "norm, counter = " << counter << " time " << time << std::endl;
}


std::mutex mtx;
void worker_mtx()
{
    for(int i=0; i<loop_count; i++)
    {
        mtx.lock();

        counter.fetch_add(1);

        mtx.unlock();
    }
}

int test_mutex()
{
    counter = 0;

    auto start = std::chrono::high_resolution_clock::now();
    std::thread t1(worker_mtx);
    std::thread t2(worker_mtx);

    t1.join();
    t2.join();

    auto end = std::chrono::high_resolution_clock::now();

    int time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

    std::cout << "mutex, counter = " << counter << " time " << time << std::endl;
}

std::atomic<int> atomic_counter;

void worker_atom()
{
    for(int i=0; i<loop_count; i++)
    {
        atomic_counter++;
    }
}

int test_atom()
{
    atomic_counter = 0;

    auto start = std::chrono::high_resolution_clock::now();
    std::thread t1(worker_atom);
    std::thread t2(worker_atom);

    t1.join();
    t2.join();

    auto end = std::chrono::high_resolution_clock::now();

    int time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

    std::cout << "atomic, counter = " << atomic_counter << " time " << time << std::endl;
}

int main()
{
    test_norm();
    test_mutex();
    test_atom();
}
