#include <memory.h>
#include <atomic>
#include <assert.h>
#include <iostream>


class ArrayOfItems
{
public:
    struct Entry
    {
        std::atomic< uint32_t > key;
        std::atomic< uint32_t > value;
    };

private:
    Entry* m_entries;
    uint32_t m_arraySize;

public:
    ArrayOfItems(uint32_t arraySize)
    {
        // Initialize cells
        m_arraySize = arraySize;
        m_entries = new Entry[arraySize];
        Clear();
    }

    ~ArrayOfItems()
    {
        // Delete cells
        delete[] m_entries;
    }

    void SetItem(uint32_t key, uint32_t value)
    {
        assert(key != 0);
        assert(value != 0);

        for (uint32_t idx = 0;; idx++)
        {
            uint32_t prevKey = 0;

            m_entries[idx].key.compare_exchange_strong(prevKey, key);
            if ((prevKey == 0) || (prevKey == key))
            {
                m_entries[idx].value.store(value);
                return;
            }
        }
    }

    uint32_t GetItem(uint32_t key)
    {
        assert(key != 0);

        for (uint32_t idx = 0;; idx++)
        {
            uint32_t probedKey = m_entries[idx].key.load();

            if (probedKey == key)
                return m_entries[idx].value.load();

            if (probedKey == 0)
                return 0;
        }
    }

    uint32_t GetItemCount()
    {
        for (uint32_t idx = 0;; idx++)
        {
            if ((m_entries[idx].key == 0
                || (m_entries[idx].value) == 0))
                return idx;
        }
    }

    void Clear()
    {
        memset(m_entries, 0, sizeof(Entry) * m_arraySize);
    }
};

