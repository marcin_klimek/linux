#include "arrayofitems.h"
#include <vector>
#include <iostream>
#include <thread>

const u_int num_items = 1000;

ArrayOfItems ar(num_items);

void worker1()
{
    for (u_int i=1; i<501; ++i )
    {
        ar.SetItem(i, i);
    }
}


void worker2()
{
    for (u_int i=501; i<=1000; ++i )
    {
        ar.SetItem(i, i);
    }
}

int main()
{
    while(true)
    {
        std::thread t1(worker1);
        std::thread t2(worker2);
        t1.join();
        t2.join();

        for (u_int i=1; i<=num_items; ++i )
        {
            if ( ar.GetItem(i) != i)
            {
               std::cout << i << " " << ar.GetItem(i) << std::endl;
            }
        }
        ar.Clear();
        std::cout << "----" << std::endl;
    }
    return 0;
}

